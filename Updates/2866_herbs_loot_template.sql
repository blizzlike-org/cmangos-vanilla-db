DELETE FROM reference_loot_template_names WHERE entry BETWEEN 12009 AND 12014;
INSERT INTO reference_loot_template_names (entry, name) VALUES
(12009, 'Purple Lotus'),
(12010, 'Sungrass'),
(12011, 'Blindweed'),
(12012, 'Golden Sansam'),
(12013, 'Dreamfoil'),
(12014, 'Mountain Silversage');
